// Uncomment these imports to begin using these cool features!

import {inject} from '@loopback/core';
import {LoggingBindings, WinstonLogger} from '@loopback/logging';
import {Filter, repository} from '@loopback/repository';
import {get, param, post, requestBody} from '@loopback/rest';
import {Todo, TodoList} from '../models';
import {TodoListRepository} from '../repositories';

// import {inject} from '@loopback/core';

export class TodoListTodoController {
  constructor(
    @repository(TodoListRepository)
    protected todoListRepository: TodoListRepository,
    // Inject a winston logger
    @inject(LoggingBindings.WINSTON_LOGGER)
    private logger: WinstonLogger,
  ) {}

  @post('/todo-lists/{id}/todos')
  async create(
    @param.path.string('id') todoListId: typeof TodoList.prototype.id,
    @requestBody() todo: Todo,
  ): Promise<Todo> {
    return this.todoListRepository.todos(todoListId).create(todo);
  }

  @get('/todo-lists/{id}/todos')
  // @logInvocation()
  async find(
    @param.path.string('id') todoListId: typeof TodoList.prototype.id,
    @param.query.object('filter') filter?: Filter<Todo>,
  ): Promise<Todo[]> {
    this.logger.debug('Fucking Debug ============> ' + todoListId);
    this.logger.info('Fucking Info =================>' + todoListId);
    return this.todoListRepository.todos(todoListId).find(filter);
  }
}
