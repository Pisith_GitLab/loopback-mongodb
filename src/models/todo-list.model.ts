import {Entity, hasMany, model, property} from '@loopback/repository';
import {Todo} from '.';

@model({settings: {strict: false}})
export class TodoList extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  title: string;

  // Relationship
  @hasMany(() => Todo, {keyTo: 'todoListId'})
  todos?: Todo[];

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TodoList>) {
    super(data);
  }
}

export interface TodoListRelations {
  // describe navigational properties here
}

export type TodoListWithRelations = TodoList & TodoListRelations;
